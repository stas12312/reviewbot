import telebot
import os
import dropbox
import secrets
import redis
import io

from flask import Flask, request, abort

BOT_TOKEN = os.environ.get("BOT_TOKEN")

bot = telebot.TeleBot(BOT_TOKEN, "")  # Объект для работы с Telegram API
dbx = dropbox.Dropbox(os.environ.get("DROPBOX_TOKEN"))  # Объект для работы с Dropbox API
redisClient = redis.StrictRedis(host='redis', port=6379, db=0)  # Объект для работы с Redis хранилищем

app = Flask(__name__)


TEXT = 0
VIDEO = 1
AUDIO = 2

def get_message_id(update):
    """
    Добавление id чата, группы или канала в Redis хранилище
    """
    chat_id = None
    try:
        message = update.message
        chat_id = message.chat.id
    except:
        message = update.channel_post
        chat_id = message.chat.id
    if chat_id:
        redisClient.sadd('U', chat_id)


def send_review(data, type=TEXT, **kwargs):
    """
    Отправка отзыва в Telegram bot
    """
    if type == TEXT:
        send = bot.send_message
    elif type == VIDEO:
        send = bot.send_video
    elif type == AUDIO:
        send = bot.send_audio

    for user in redisClient.smembers('U'):
        try:
            chat_id = int(user)
            send(chat_id, data, **kwargs)
        except telebot.apihelper.ApiException:
            redisClient.srem('U', user)


@app.route('/webhook/' + BOT_TOKEN, methods=['POST'])
def reception_update():
    """
    Принятие webhook от Telegram
    """
    if request.headers.get('content-type') == 'application/json':
        json_string = request.get_data().decode('utf-8')
        update = telebot.types.Update.de_json(json_string)

        with open("logs_bot.txt", 'a+') as f:
            f.write(json_string)

        get_message_id(update)

        bot.process_new_updates([update])
        return 'OK'
    else:
        abort(403)


@bot.message_handler(commands=['start'])
def processing_start(message):
    """
    Добавление пользователя в список для рассылки
    """
    bot.send_message(message.chat.id, 'Вы подписались на отзывы')
    redisClient.sadd('U', message.chat.id)


@app.route('/send/<path:path_to_file>')
def send_file(path_to_file):
    """
    Принятие запроса на отправку файла в Telegram bot
    """
    ext = path_to_file.split('.')[-1]
    branch = path_to_file.split('/')[-1].split('-')[0]
    _ , res = dbx.files_download(path='/'+path_to_file)

    data = res.content
    if ext == 'mp4':  # Если видео
        send_review(data, type=VIDEO, caption=f'Отзыв {branch}')
    elif ext == 'mp3':  # Если аудио
        send_review(data, type=AUDIO ,title=f'Отзыв {branch}', duration=None)
    elif ext == 'txt':  # Если текст
        text = f'<b>Отзыв {branch}:</b>\n' + data.decode()
        send_review(text, type=TEXT, parse_mode='HTML')
    
    return 'Файл успешно отправлен'

@app.route('/get_all_users')
def get_all_users():
    """
    Получение списка пользователей
    """
    users = redisClient.smembers('U')
    users_str = 'Пользователи:<br>'
    for user in users:
        users_str += user.decode() + '<br>'
    return users_str
